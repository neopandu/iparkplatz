package org.the_ex.parkplatz;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.text.DecimalFormat;

/**
 * Created by Fungki Pandu on 07/02/2018.
 */

public class WarningDetectedObject {
    private double latitude, longitude, distance;
    private String placename;
    private int id;

    public WarningDetectedObject(int id, String placename, double latitude, double longitude, double distance) {
        this.id = id;
        this.placename = placename;
        this.latitude = latitude;
        this.longitude = longitude;
        this.distance = distance;
    }

    public int getId() {
        return id;
    }

    public String getPlacename() {
        return placename;
    }

    public double getDistance() {
        return distance;
    }

    public String getDistanceText() {
        if (distance>1000)
            return Double.parseDouble((new DecimalFormat("#.####")
                    .format(distance/1000)).replace(",",".")) + " km";
        else
            return Double.parseDouble((new DecimalFormat("#.####")
                    .format(distance)).replace(",",".")) + " m";
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public Location getLocation(){
        Location location = new Location(placename);
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        return location;
    }

    public LatLng getLatLng(){
        return new LatLng(latitude, longitude);
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public void setPlacename(String placename) {
        this.placename = placename;
    }
}
