package org.the_ex.parkplatz;

import android.Manifest;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ParkplatzActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    List<Marker> markerList = new ArrayList<>();

    private static final int PERMISSIONS_REQUEST_USER_LOCATION = 591;
    Location mLastLocation;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;

    NotificationManager notificationManager;

    private final static int REQUEST_CHECK_SETTINGS_GPS=0x1;

    private GoogleMap mMap;
    private BottomSheetBehavior mBottomSheetBehavior;
    private RecyclerView recyclerView;
    private RVAdapter rvAdapter;
    private MapView mapMain;

    MqttHelper mqttHelper;
    private String serverAddress = "tcp://ngehubx.online:1883";
    private String usernameMqtt = "admintes";
    private String passwordMqtt = "admin123";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parkplatz);

        final View bottomSheet = findViewById(R.id.bottom_sheet);

        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);

        mBottomSheetBehavior.setPeekHeight(250);
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        mqttHelper = new MqttHelper(this);
        mqttHelper.mqttConnect(serverAddress, usernameMqtt, passwordMqtt);

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        recyclerView = findViewById(R.id.rvList);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        rvAdapter = new RVAdapter(new ArrayList<WarningDetectedObject>());
        recyclerView.setAdapter(rvAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView,
                new RecyclerTouchListener.ClickListener() {
                    @Override
                    public void onClick(View view, final int position) {

                        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                        mMap.animateCamera(CameraUpdateFactory.newLatLng(rvAdapter.getItem(position).getLatLng()),
                                new GoogleMap.CancelableCallback() {
                                    @Override
                                    public void onFinish() {
                                        animateMarker(markerList.get(findMarkerPositionById(rvAdapter.getItem(position).getId())));
                                    }

                                    @Override
                                    public void onCancel() {

                                    }
                                }
                        );


                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                }));

        mBottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
             public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    mBottomSheetBehavior.setPeekHeight(250);
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        mapMain = findViewById(R.id.map);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT
        );
        params.setMargins(0, 0, 0, 250);
        mapMain.setLayoutParams(params);

        try {
            mapMain.onCreate(savedInstanceState);
            mapMain.getMapAsync(this);
        } catch (Exception e) {
            Log.d("Map Error", e.getMessage());
        }

        buildGoogleApiClient();
    }

    synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.i("GoogleMap", "MapReady");
        mMap = googleMap;
        mapMain.onResume();

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);

        //handle MQTT
        mqttHelper.setListener(new MqttHelper.Listener() {
            @Override
            public void onMessageArrived(String topic, String message) {
                try {
                    JSONObject nodeObj = new JSONObject(message);
                    Log.i("MQTT", "onMessageArrived: " + message);
                    if (nodeObj.getInt("status")==1) {
                        if (!rvAdapter.isIdAvailable(nodeObj.getInt("id"))) {
                            addWarnLocation(nodeObj.getInt("id"),
                                    nodeObj.getDouble("latitude"),
                                    nodeObj.getDouble("longitude"));
                        }
                    } else {
                        removeWarnLocation(nodeObj.getInt("id"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onConnected() {
                mqttHelper.mqttSubscribe("ipark/data", 0);
            }
        });
    }


    private void animateMarker(final Marker marker) {
        // This causes the marker to bounce into position when it is clicked.
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final long duration = 1000;

        final Interpolator interpolator = new BounceInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float interpolation = 1 - interpolator.getInterpolation((float) elapsed / duration);
                float t = Math.max(interpolation, 0);
                float anchorv1 = 0.5f;
                float anchorv2kons = 1.0f;
                float anchorv2 = anchorv2kons + 2*t;
                marker.setAnchor(anchorv1, anchorv2);

                if (t > 0.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    private void addWarnLocation(int id, double latitude, double longitude) {
        String addressName = getAddressName(latitude, longitude);
        Location warnLocation = new Location(addressName);
        warnLocation.setLatitude(latitude);
        warnLocation.setLongitude(longitude);
        double distanceFromUser = 0;
        if (mLastLocation!=null) {
            distanceFromUser = mLastLocation.distanceTo(warnLocation);
        }

        WarningDetectedObject warningDetectedObject =
                new WarningDetectedObject(id, addressName, latitude, longitude, distanceFromUser);

        rvAdapter.addItem(warningDetectedObject);
        Marker newMarker = mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_dont_park_marker ))
                .position(new LatLng(latitude, longitude))
                .title(addressName));

        markerList.add(newMarker);
        animateMarker(newMarker);

        showNotif(id, latitude, longitude, addressName, warningDetectedObject.getDistanceText());
    }

    private int findMarkerPositionById(int id){
        WarningDetectedObject warnObject = rvAdapter.getItemById(id);
        if (warnObject != null) {
            for (int i = 0; i < markerList.size(); i++) {
                if (markerList.get(i).getTitle().equals(warnObject.getPlacename())) {
                    return i;
                }
            }
        }
        return -1;
    }

    private void removeWarnLocation(int id){
        notificationManager.cancel(id);
        int position = findMarkerPositionById(id);
        if (position >= 0){
            markerList.get(position).remove();
            markerList.remove(position);
        }
        rvAdapter.removeItemById(id);
    }

    private String getAddressName(double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        List<Address> addresses;
        String alamat = "Unknown";
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);

            if (addresses != null && addresses.size() > 0) {
                System.out.println(addresses);
                alamat = addresses.get(0).getAddressLine(0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return alamat;
    }

    @Override
    public void onStart(){
        super.onStart();
        if(mGoogleApiClient != null){
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        final int deviceId = intent.getIntExtra("parkLocId", -1);
        if (deviceId < 0) return;
        if (!rvAdapter.isIdAvailable(deviceId)){
            Toast.makeText(this, "Parkir liar sudah ditertibkan", Toast.LENGTH_SHORT).show();
        }

        mMap.animateCamera(CameraUpdateFactory.newLatLng(rvAdapter.getItemById(deviceId).getLatLng()),
                new GoogleMap.CancelableCallback() {
                    @Override
                    public void onFinish() {
                        animateMarker(markerList.get(findMarkerPositionById(rvAdapter.getItemById(deviceId).getId())));
                    }

                    @Override
                    public void onCancel() {

                    }
                }
        );
    }

    @Override
    public void onBackPressed() {
        //sembunyikan apps di latar belakang
        this.moveTaskToBack(true);
        //dikomen aja, biar apps nya gak ketutup
        //super.onBackPressed();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.d("mGoogleApiClient", "Connected");
        checkAccessLocationPermission();
        getMyLocation();

        addWarnLocation(2, -7.932514, 112.613803);
        addWarnLocation(1, -7.9314836, 112.6145853);
    }

    private void getMyLocation(){
        if(mGoogleApiClient!=null) {
            if (mGoogleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(
                        ParkplatzActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                    if (mLastLocation != null){
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                                new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), 15
                        ));
                    }
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(2000);
                    locationRequest.setFastestInterval(2000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
                    PendingResult result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback() {
                        @Override
                        public void onResult(@NonNull Result result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:
                                    int permissionLocation = ContextCompat
                                            .checkSelfPermission(ParkplatzActivity.this,
                                                    Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                                        if (mLastLocation != null){
                                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                                                    new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()),
                                                    15
                                            ));
                                        }
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                    // Location settings are not satisfied.
                                    // But could be fixed by showing the user a dialog.
                                    try {
                                        // Show the dialog by calling startResolutionForResult(),
                                        // and check the result in onActivityResult().
                                        // Ask to turn on GPS automatically
                                        status.startResolutionForResult(ParkplatzActivity.this, REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {
                                        // Ignore the error.
                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                    // Location settings are not satisfied. However, we have no way to fix the
                                    // settings so we won't show the dialog.
                                    finish();
                                    break;
                            }
                        }
                    });
                }
            }
        }
    }

    public void checkAccessLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSIONS_REQUEST_USER_LOCATION);
        }
    }

    private void showNotif(int deviceId, double latitude, double longitude, String addressName, String distance){

        String kontenPesan = "Parkir liar terdeteksi "+ distance + " dari lokasi Anda. Lokasi di "+ addressName;

        Intent notifIntent = new Intent(this, ParkplatzActivity.class).putExtra("parkLocId", deviceId);

        PendingIntent pIntent = PendingIntent.getActivity(this, 0, notifIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification mNotif = new NotificationCompat.Builder(this)
                .setAutoCancel(true)
                .setContentTitle("Parkir Liar terdeteksi!")
                .setContentText(kontenPesan)
                .setSmallIcon(R.drawable.ic_no_parking_notif)
                .setContentIntent(pIntent)
                .setPriority(Notification.PRIORITY_HIGH)
                .setShowWhen(true)
                .build();

        mNotif.defaults = Notification.DEFAULT_LIGHTS;
        mNotif.defaults = Notification.DEFAULT_SOUND;
        mNotif.flags |= Notification.FLAG_SHOW_LIGHTS;

        Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(300);      //atur lamanya terjadi getaran dalam milisecond

        notificationManager.notify(deviceId, mNotif);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_USER_LOCATION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    // permission denied, boo! Disable the
                     //create GPS notif dialog
                    new AlertDialog.Builder(this)
                            .setIcon(R.mipmap.ic_gps_off)
                            .setTitle(R.string.notif_gps_no_permission_title)
                            .setMessage(getString(R.string.notif_gps_no_permission_body))
                            .setPositiveButton(R.string.o_k_, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    checkAccessLocationPermission();
                                }
                            })
                            .setNegativeButton(getString(R.string.exit), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    ParkplatzActivity.this.finish();
                                }
                            })
                            .setCancelable(false)
                            .show();
                }
                break;
            case Activity.RESULT_OK:
                getMyLocation();
                break;
            case Activity.RESULT_CANCELED:
                finish();
                break;
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
//        Log.d("GPS", "LocationChanged");

        if (mLastLocation == null) {
            mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude())));
        }

        mLastLocation = location;

        if (rvAdapter.getItemCount() > 0){
            for (int i = 0; i < rvAdapter.getItemCount(); i++) {
                rvAdapter.updateDistanceItem(i, location.distanceTo(rvAdapter.getItem(i).getLocation()));
            }
        }
    }
}