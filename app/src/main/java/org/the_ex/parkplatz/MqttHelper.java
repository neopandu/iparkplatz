package org.the_ex.parkplatz;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.io.UnsupportedEncodingException;

/**
 * Created by Fungki Pandu on 17/10/2017.
 */

public class MqttHelper {

    private Context context;
    private MqttAndroidClient client;

    private Listener listener;

    public MqttHelper(Context context) {
        this.context = context;
    }

    void mqttConnect(String serverAddress, String usernameMqtt, String passwordMqtt) {
        String clientId = MqttClient.generateClientId();
        client = new MqttAndroidClient(context, serverAddress, clientId);
        Log.d("MQTT", "Connecting...");

        MqttConnectOptions options = new MqttConnectOptions();
        options.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1);
        options.setUserName(usernameMqtt);
        options.setPassword(passwordMqtt.toCharArray());
        try {
            IMqttToken token = client.connect(options);
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    Log.d("MQTT", "onSuccess" + asyncActionToken);
                    Toast.makeText(context, "MQTT Connected", Toast.LENGTH_SHORT).show();

                    client.setCallback(new MqttCallback() {
                        @Override
                        public void connectionLost(Throwable cause) {
                            Log.e("mqttLost", "connectionLost: " + cause.getCause());
                        }

                        @Override
                        public void messageArrived(String topic, MqttMessage message) throws Exception {
                            byte[] pesan = message.getPayload();
                            String msg = "";
                            for (byte aPesan : pesan) {
                                msg += String.valueOf(Character.toChars(aPesan));
                            }

                            Log.d("msgArrived", topic + " : " + message);
                            listener.onMessageArrived(topic, msg);
                        }

                        @Override
                        public void deliveryComplete(IMqttDeliveryToken token) {
                            //Called when a outgoing publish is complete
                        }
                    });

                    listener.onConnected();
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Log.d("MQTT", "onFailure "+exception.getMessage());
                    Toast.makeText(context, "Connect on Failure : " + exception.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
            Toast.makeText(context, "Error ", Toast.LENGTH_SHORT).show();
        }
    }

    public void mqttUnsubscribe(String topic){
        try {
            client.unsubscribe(topic);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void mqttDisconnect(){
        try {
            client.disconnect();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void mqttPublish(String payload, String topic) {
        byte[] encodedPayload;
        try {
            encodedPayload = payload.getBytes("UTF-8");
            MqttMessage message = new MqttMessage(encodedPayload);
            client.publish(topic, message);
        } catch (UnsupportedEncodingException | MqttException e) {
            e.printStackTrace();
        }
    }

    interface Listener{
        void onMessageArrived(String topic, String message);
        void onConnected();
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void mqttSubscribe(String topic, int qos) {
        try {
            IMqttToken subTokenSuhu = client.subscribe(topic, qos);
            subTokenSuhu.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    String[] topics = asyncActionToken.getTopics();
                    String s = "";
                    for (String topic: topics) {
                        s += topic +", ";
                    }
                    Log.d("log", "subscribe on " + s);
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    Toast.makeText(context, "subbed on Failure : " + asyncActionToken,
                            Toast.LENGTH_LONG).show();
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }
}
