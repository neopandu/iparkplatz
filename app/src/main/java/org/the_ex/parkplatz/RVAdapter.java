package org.the_ex.parkplatz;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.warningViewHolder> {

    private List<WarningDetectedObject> warnlist/* = new ArrayList<>()*/;
    private Context context;

    public RVAdapter(List<WarningDetectedObject> warnlist) {
        this.warnlist = warnlist;
    }

    @Override
    public warningViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.warning_item, parent, false);
        context = parent.getContext();

        return new warningViewHolder(v);
    }

    @Override
    public void onBindViewHolder(warningViewHolder holder, int position) {
        WarningDetectedObject warn = warnlist.get(position);
        holder.tvCoordinate.setText("("+ warn.getLatitude() +", "+ warn.getLongitude() +")");
        holder.tvPlaceName.setText(warn.getPlacename());
        holder.tvDistance.setText(warn.getDistanceText());
    }

    public boolean isIdAvailable(int id){
        for (int i = 0; i < warnlist.size(); i++) {
            if (warnlist.get(i).getId()==id){
                return true;
            }
        }
        return false;
    }

    @Override
    public int getItemCount() {
        return warnlist.size();
    }

    public WarningDetectedObject getItemById(int id){
        for (int position = 0; position < warnlist.size(); position++) {
            if (warnlist.get(position).getId() == id){
                return warnlist.get(position);
            }
        }
        return null;
    }

    public WarningDetectedObject getItem(int position) {
        return warnlist.get(position);
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void addItem(WarningDetectedObject mWarn) {
        this.warnlist.add(mWarn);

        Collections.sort(warnlist, new Comparator<WarningDetectedObject>() {
            @Override
            public int compare(WarningDetectedObject o1, WarningDetectedObject o2) {
                return Double.compare(o1.getDistance(),o2.getDistance());
            }
        });

        notifyDataSetChanged();
    }

    public void updateDistanceItem(int index, double distance){
        warnlist.get(index).setDistance(distance);
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        warnlist.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, warnlist.size());
    }

    public boolean removeItemById(int id) {
        for (int position = 0; position < warnlist.size(); position++) {
            if (warnlist.get(position).getId() == id){
                warnlist.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, warnlist.size());
                return true;
            }
        }
        return false;
    }

    public void renewItem(List<WarningDetectedObject> newHistories) {
        this.warnlist = newHistories;
    }

    static class warningViewHolder extends RecyclerView.ViewHolder {

        TextView tvPlaceName, tvCoordinate, tvDistance;

        warningViewHolder(View itemView) {
            super(itemView);
            tvPlaceName = itemView.findViewById(R.id.tv_placename);
            tvCoordinate = itemView.findViewById(R.id.tv_coordinate);
            tvDistance = itemView.findViewById(R.id.tv_distance);
        }
    }
}
